<nav class="navbar">
    <div class="container">
        <a href="/" class="navbar-brand">Toko Online</a>
        <ul class="navbar-links">
            <li>
                <a href="./add.php" <?= $_SERVER["REQUEST_URI"] == "/add.php"
                    ? "class='active'"
                    : null ?>>
                    Tambah
                </a>
            </li>
            <li>
                <a href="./search.php" <?= $_SERVER["REQUEST_URI"] ==
                "/search.php"
                    ? "class='active'"
                    : null ?>>
                    Cari
                </a>
            </li>
            <li>
                <a href="./customer.php" <?= $_SERVER["REQUEST_URI"] ==
                "/customer.php"
                    ? "class='active'"
                    : null ?>>
                    Pelanggan
                </a>
            </li>
            <li>
                <a href="./cart.php" <?= $_SERVER["REQUEST_URI"] == "/cart.php"
                    ? "class='active'"
                    : null ?>>
                    Keranjang Belanja
                </a>
            </li>
        </ul>
    </div>
</nav>