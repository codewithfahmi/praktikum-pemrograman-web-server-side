-- Adminer 4.8.1 MySQL 8.0.33 dump

SET NAMES utf8;
SET time_zone = '+00:00';
SET foreign_key_checks = 0;

SET NAMES utf8mb4;

DROP DATABASE IF EXISTS `toko_ol`;
CREATE DATABASE `toko_ol` /*!40100 DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci */ /*!80016 DEFAULT ENCRYPTION='N' */;
USE `toko_ol`;

CREATE TABLE `barang` (
  `idbarang` int NOT NULL AUTO_INCREMENT,
  `nama` varchar(40) NOT NULL,
  `harga` int NOT NULL DEFAULT '0',
  `stok` int NOT NULL DEFAULT '0',
  `foto` text NOT NULL,
  PRIMARY KEY (`idbarang`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;


CREATE TABLE `djual` (
  `iddjual` int NOT NULL AUTO_INCREMENT,
  `idhjual` int NOT NULL,
  `idbarang` int NOT NULL,
  `qty` int NOT NULL,
  `harga` int NOT NULL,
  PRIMARY KEY (`iddjual`),
  KEY `idhjual` (`idhjual`),
  KEY `idbarang` (`idbarang`),
  CONSTRAINT `djual_ibfk_1` FOREIGN KEY (`idhjual`) REFERENCES `hjual` (`idhjual`),
  CONSTRAINT `djual_ibfk_2` FOREIGN KEY (`idbarang`) REFERENCES `barang` (`idbarang`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;


CREATE TABLE `hjual` (
  `idhjual` int NOT NULL AUTO_INCREMENT,
  `tanggal` date NOT NULL,
  `namacust` varchar(40) NOT NULL,
  `email` varchar(70) NOT NULL,
  `notelp` varchar(40) NOT NULL,
  PRIMARY KEY (`idhjual`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;


-- 2023-06-13 17:18:09