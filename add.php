<?php require_once "./connections.php"; ?>
<?php $title = "Tambah Barang"; ?>
<?php require_once "./header.php"; ?>

<header>
    <h2>Tambah Barang</h2>
</header>
<main>
    <?php
    session_start();
    $errors = $_SESSION["errors"] ?? null;
    ?>
    <form method="POST" action="./save.php" enctype="multipart/form-data">
        <table class="undefault bordered">
            <tr>
                <th>Nama Barang</th>
                <td>
                    <input type="text" name="name" id="name">
                </td>
            </tr>
            <tr>
                <th>Harga</th>
                <td>
                    <input type="tel" name="price" id="price">
                </td>
            </tr>
            <tr>
                <th>Stok</th>
                <td>
                    <input type="number" name="quantity" id="quantity">
                </td>
            </tr>
            <tr>
                <th>Foto Produk</th>
                <td>
                    <input type="file" name="picture" id="picture">
                </td>
            </tr>
            <tr>
                <td><!-- empty --></td>
                <td>
                    <button name="save" value="save">Simpan Barang</button>
                </td>
            </tr>
        </table>
    </form>
</main>

<?php require_once "./footer.php"; ?>
