<?php
if (isset($_POST["save"])) {
    $buy_cust_name = trim($_POST["name"]);
    $buy_cust_email = trim($_POST["email"]);
    $buy_cust_telephone = trim($_POST["telephone"]);
    $buy_cust_date = date("Y-m-d");
    $buy_cust_shopping_cart = "";
    $buy_qty = 1;

    $errors = [];

    if (empty($buy_cust_name)) {
        $errors[] = "Nama pelanggan tidak boleh kosong";
    }

    if (empty($buy_cust_email)) {
        $errors[] = "Email pelanggan tidak boleh kosong";
    }

    if (empty($buy_cust_telephone)) {
        $errors[] = "Nomor telefon pelanggan tidak boleh kosong";
    }

    if (isset($_COOKIE["keranjang_belanja"])) {
        $buy_cust_shopping_cart = $_COOKIE["keranjang_belanja"];
    } else {
        $errors[] = "Keranjang belanja kosong";
    }

    if (count($errors) > 0) {
        $title = "Kesalahan pada penambahan pelanggan";
        require_once "./header.php";

        $alert = "<h3>$title</h3>";
        $alert .= "<ul>";

        foreach ($errors as $error) {
            $alert .= "<li>" . ucfirst($error) . "</li>";
        }

        $alert .= "</ul>";

        echo $alert;
        echo "<a href='./customer.php'>Kembali</a>";
    } else {
        require_once "./connections.php";
        $transaction = $db->begin_transaction();
        try {
        } catch (mysqli_sql_exception $exception) {
        }
        echo "Data siap disimpan";
        setcookie("keranjang_belanja", $buy_cust_shopping_cart, time() - 3600);
    }
}
?>
