<?php $title = "Pencarian Barang"; ?>
<?php require_once "./header.php"; ?>
<header>
  <h2>Pencarian Barang</h2>
</header>
<main>
  <form method="POST" action="./index.php">
    <table class="undefault bordered">
      <tr>
        <td>
          <label style="display:block;margin-bottom:4px">Kata Kunci</label>
          <input type="text" name="keyword" id="keyword" style="margin-bottom: 12px">
          <button type="submit">Cari Barang</button>
        </td>
      </tr>
    </table>
  </form>
</main>
<?php require_once "./footer.php"; ?>
