<?php $title = "Data Pembeli Barang"; ?>
<?php require_once "./header.php"; ?>
<section>
  <header>
    <h2>Data Pembeli Barang</h2>
  </header>
  <main>
    <form method="POST" action="./buy.php">
      <table class="undefault bordered">
        <tr>
          <th>Nama Pelanggan</th>
          <td><input type="text" name="name" id="name"></td>
        </tr>
        <tr>
          <th>Email</th>
          <td><input type="email" name="email" id="email"></td>
        </tr>
        <tr>
          <th>Nomor Telefon</th>
          <td><input type="tel" name="telephone" id="telephone"></td>
        </tr>
        <tr>
          <td></td>
          <td><button type="submit" name="save" value="save">Simpan Pembeli</button></td>
        </tr>
      </table>
    </form>
  </main>
</section>
<section>
  <?php require_once "./cart.php"; ?>
</section>
<?php require_once "./footer.php"; ?>
