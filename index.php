<?php

$title = "Beranda";
require_once "./connections.php";
require_once "./header.php";
?>

<header>
    <h2>Daftar Barang Tersedia</h2>
</header>

<?php
$shopping_cart = 0;
if (isset($_COOKIE["keranjang_belanja"])) {
    $shopping_cart = $_COOKIE["keranjang_belanja"];
}

if (isset($_GET["id"])) {
    $id = $_GET["id"];
    $shopping_cart = $shopping_cart . "," . $id;
    setcookie("keranjang_belanja", $shopping_cart, time() + 3600);
}

$keyword = $_POST["keyword"] ?? null;
$query =
    "SELECT * FROM barang WHERE 1=1 AND id not in (" .
    $shopping_cart .
    ") AND quantity > 0";

if ($keyword != null) {
    $query .= " AND name LIKE '%" . $keyword . "%'";
}

$query .= " ORDER BY id DESC";

$get_data_barang = $db->query($query);
$get_count_barang = $get_data_barang->num_rows;
?>

<main>
    <?php if ($get_count_barang > 0): ?>
        <p>Jumlah barang
            <?= "(" . $get_count_barang . ")" ?>
        </p>
        <table class="bordered">
            <thead>
                <tr>
                    <th>Foto</th>
                    <th>Nama Barang</th>
                    <th>Harga Jual</th>
                    <th>Stok</th>
                    <th>Operasi</th>
                </tr>
            </thead>
            <tbody>
                <?php foreach ($get_data_barang as $data): ?>
                    <tr>
                        <td>
                            <a href="./picture/<?= $data["picture"] ?>">
                                <img src="./thumbnail/<?= $data["picture"] ?>">
                            </a>
                        </td>
                        <td>
                            <?= ucwords($data["name"]) ?>
                        </td>
                        <td>
                            Rp.
                            <?= number_format($data["price"]) ?>
                        </td>
                        <td>
                            <?= $data["quantity"] ?>
                        </td>
                        <td>
                            <!-- <a href="./detail.php?id=<?= $data[
                                "id"
                            ] ?>">🕵 Lihat</a> &nbsp;
                            <a href="./edit.php?id=<?= $data[
                                "id"
                            ] ?>">📝 Edit</a> &nbsp;
                            <a href="./delete.php?id=<?= $data[
                                "id"
                            ] ?>">🗑️ Hapus</a> -->
                            <a href="<?= $_SERVER["PHP_SELF"] .
                                "?id=" .
                                $data["id"] ?>">
                                🛒 Beli
                            </a>
                        </td>
                    </tr>
                <?php endforeach; ?>
            </tbody>
        </table>
    <?php else: ?>
        <?php if (isset($_POST["keyword"])): ?>
            <p>Data yang dicari tidak ditemukan</p>
        <?php else: ?>
            <p>Belum ada data barang yang ditambahkan</p>
        <?php endif; ?>
    <?php endif; ?>
</main>

<?php require_once "./footer.php"; ?>
