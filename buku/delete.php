<?php $title = "Hapus buku" ?>
<?php require_once "./header.php" ?>
<?php
require_once "./connections.php";
$id = $_GET['id'] ?? die("tidak dapat melakukan operasi hapus buku");
$buku = $db->query("SELECT * FROM buku WHERE id = '$id'")->fetch_array(MYSQLI_BOTH);
?>

<?php
if (isset($_GET['delete'])) {
    $is_delete = $_GET['delete'];
    if ($is_delete) {
        unlink("./cover/" . $buku['picture']);
        unlink("./thumbnail/" . $buku['picture']);

        $delete = $db->query("DELETE FROM buku WHERE id = '$id'");
        if ($delete) {
            echo "<header><h3>✅ buku berhasil dihapus</h3></header>";
            echo "<a href='./index.php'>Daftar buku</a>";
        }
    }
} else {
    ?>
    <header>
        <h2>Hapus buku</h2>
    </header>
    <main>
        <table class="bordered undefault">
            <tr>
                <td colspan="2" style="text-align: center">
                    <a href="./cover/<?= $buku['cover'] ?>">
                        <img src="./cover/<?= $buku['cover'] ?>" width="250">
                    </a>
                </td>
            </tr>
            <tr>
                <th>Judul</th>
                <td>
                    <?= ucwords($buku['title']) ?>
                </td>
            </tr>
            <tr>
                <th>Stok</th>
                <td>
                    <?= ($buku['quantity']) ?>
                </td>
            </tr>
            <tr>
                <th>Pengarang</th>
                <td>
                    <?= $buku['author'] ?>
                </td>
            </tr>
            <tr>
                <th>Penerbit</th>
                <td>
                    <?= $buku['publisher'] ?>
                </td>
            </tr>
            <tr>
                <th>Hapus buku ini?</th>
                <td>
                    <a href="./delete.php?id=<?= $buku['id'] ?>&delete=true">
                        🗑️ Hapus buku
                    </a> &nbsp;
                    <a href="./index.php">
                        ❌ Batal
                    </a>
                </td>
            </tr>
        </table>
    </main>
    <?php
}
?>

<?php require_once "./footer.php" ?>