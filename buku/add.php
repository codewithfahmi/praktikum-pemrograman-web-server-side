<?php require_once "./connections.php" ?>
<?php $title = "Tambah Barang" ?>
<?php require_once "./header.php" ?>

<header>
  <h2>Tambah Buku</h2>
</header>
<main>
  <form method="POST" action="./save.php" enctype="multipart/form-data">
    <table class="undefault bordered">
      <tr>
        <th>Kode</th>
        <td>
          <input type="text" name="code" id="code" readonly>
        </td>
      </tr>
      <tr>
        <th>Judul</th>
        <td>
          <input type="text" name="title" id="title">
        </td>
      </tr>
      <tr>
        <th>Pengarang</th>
        <td>
          <input type="text" name="author" id="author">
        </td>
      </tr>
      <tr>
        <th>Penerbit</th>
        <td>
          <input type="text" name="publisher" id="publisher">
        </td>
      </tr>
      <tr>
        <th>Stok</th>
        <td>
          <input type="number" name="quantity" id="quantity">
        </td>
      </tr>
      <tr>
        <th>Sampul</th>
        <td>
          <input type="file" name="cover" id="cover">
        </td>
      </tr>
      <tr>
        <td><!-- empty --></td>
        <td>
          <button name="save" value="save">Simpan Barang</button>
        </td>
      </tr>
    </table>
  </form>
</main>
<script>
  const title = document.getElementById("title");
  const code = document.getElementById("code");
  title.addEventListener("keyup", function (event) {
    const splitCode = this.value.split(" ");
    let setCode = [];
    splitCode.forEach(element => {
      setCode.push(element[0]);
    });

    code.setAttribute("value", setCode.join("").toLocaleUpperCase());
  })
</script>
<?php require_once "./footer.php" ?>