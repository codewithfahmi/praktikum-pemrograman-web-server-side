<nav class="navbar">
    <div class="container">
        <a href="/" class="navbar-brand">Toko Buku</a>
        <ul class="navbar-links">
            <li>
                <a href="./add.php" <?= $_SERVER['REQUEST_URI'] == "/add.php" ? "class='active'" : null ?>>
                    Tambah
                </a>
            </li>
            <li>
                <a href="./search.php" <?= $_SERVER['REQUEST_URI'] == "/search.php" ? "class='active'" : null ?>>
                    Cari
                </a>
            </li>
            <li>
                <a href="./bracket.php" <?= $_SERVER['REQUEST_URI'] == "/bracket.php" ? "class='active'" : null ?>>
                    Keranjang
                </a>
            </li>
            <li>
                <a href="./borrow.php" <?= $_SERVER['REQUEST_URI'] == "/borrow.php" ? "class='active'" : null ?>>
                    Peminjaman
                </a>
            </li>
        </ul>
    </div>
</nav>