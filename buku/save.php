<?php
function make_thumbnail($source, $destination) {
  list($width, $height, $type) = getimagesize($source);

  $image_source = null;
  $thumbnail_width = 100;

  switch ($type) {
    case 1:
      $image_source = imagecreatefromgif($source);
      break;
    case 2:
      $image_source = imagecreatefromjpeg($source);
      break;
    case 3:
      $image_source = imagecreatefrompng($source);
      break;
  }

  $thumbnail_width_destination = 0;
  $thumbnail_height_destination = 0;

  if ($width > $thumbnail_width) {
    $thumbnail_width_destination = $thumbnail_width;
    $thumbnail_height_destination = round($thumbnail_width / $width * $height);
  } else {
    $thumbnail_width_destination = round($thumbnail_width / $height * $width);
    $thumbnail_height_destination = $thumbnail_width;
  }

  $thumbnail_destination = imagecreatetruecolor(
    $thumbnail_width_destination,
    $thumbnail_height_destination
  );
  imagecopyresampled(
    $thumbnail_destination,
    $image_source,
    0,
    0,
    0,
    0,
    $thumbnail_width_destination,
    $thumbnail_height_destination,
    $width,
    $height
  );

  imagejpeg($thumbnail_destination, $destination);
  imagedestroy($image_source);
  imagedestroy($thumbnail_destination);
}

if (isset($_POST['save'])) {
  $book_code = trim($_POST['code']);
  $book_title = trim($_POST['title']);
  $book_author = trim($_POST['author']);
  $book_quantity = trim($_POST['quantity']);
  $book_publisher = trim($_POST['publisher']);
  $book_picture = $_FILES['cover'];

  /** picture */
  $book_picture_name = $book_picture['name'];
  $book_picture_tmp_name = $book_picture['tmp_name'];
  $book_picture_size = $book_picture['size'];
  $book_picture_type = $book_picture['type'];
  $book_picture_allow_types = [
    'image/jpeg',
    'image/pjepg',
    'image/jpg',
    'image/png',
    'image/gif'
  ];
  $book_picture_size_max = 1500000;
  $book_picture_dir = "./cover/" . $book_picture_name;
  $book_picture_thumb_dir = "./thumbnail/" . $book_picture_name;

  $errors = array();

  if (empty($book_code))
    $errors[] = "kode buku tidak boleh kosong";

  if (empty($book_title))
    $errors[] = "judul buku tidak boleh kosong";

  if (empty($book_quantity))
    $errors[] = "stok buku tidak boleh kosong";

  if (empty($book_author))
    $errors[] = "pengarang buku tidak boleh kosong";

  if (empty($book_publisher))
    $errors[] = "penerbit buku tidak boleh kosong";

  if ($book_picture_size == 0)
    $errors[] = "foto buku tidak boleh kosong";

  if ($book_picture_size > 0) {
    if ($book_picture_size > $book_picture_size_max)
      $errors[] = "ukuran foto tidak boleh lebih dari 1.5MB";

    if (!in_array($book_picture_type, $book_picture_allow_types))
      $errors[] = "format foto tidak didukung";

    if (!file_exists(("./cover/")))
      mkdir("./cover/");

    if (!file_exists("./thumbnail/"))
      mkdir("./thumbnail/");

    if (!move_uploaded_file($book_picture_tmp_name, $book_picture_dir)) {
      $errors[] = "gagal unggah foto";
    } else {
      make_thumbnail($book_picture_dir, $book_picture_thumb_dir);
    }
  }

  if (count($errors) > 0) {
    $title = "Kesalahan pada menyimpan book";
    require_once "./header.php";
    $alert = "<h3>$title</h3>";
    $alert .= "<ul>";
    foreach ($errors as $error) {
      $alert .= "<li>" . ucfirst($error) . "</li>";
    }
    echo $alert . "</ul>";
    echo "<a href='./add.php'>Kembali</a>";
  } else {
    require_once "./connections.php";
    $save = $db->query("INSERT INTO buku(code, title, author, publisher, quantity, cover) 
															 VALUES(
																	'$book_code', 
																	'$book_title', 
																	'$book_author', 
																	'$book_publisher',
                                  '$book_quantity',
                                  '$book_picture_name')"
    );

    if ($save) {
      $title = "Berhasil menyimpan buku";
      require_once "./header.php";
      echo "<header><h3>✅ Buku \"$book_title\" berhasil disimpan</h3></header>";
      echo "<a href='./index.php'>Daftar Buku</a>";
    } else {
      $title = "Gagal menyimpan buku";
      require_once "./header.php";
      echo "<header><h3>❌ Gagal menyimpan buku \"$book_title\"</h3></header>";
      echo "<a href='./add.php'>Kembali</a>";
    }
  }
}
?>
<?php require_once "./footer.php" ?>