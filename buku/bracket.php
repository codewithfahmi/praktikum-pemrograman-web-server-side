<?php require_once "./connections.php" ?>

<?php $title = "Keranjang Buku"; ?>
<?php require_once "./header.php" ?>
<header>
    <h2>Keranjang Buku</h2>
</header>
<?php

if ((isset($_GET['id']) && isset($_GET['delete'])) && $_GET['delete'] == true) {
    $id = $_GET['id'];
    $return_it = $db->query("DELETE FROM sewa WHERE buku = '$id'");
    if ($return_it) {
        echo "Buku telah dikembalikan";
    } else {
        echo "Gagal mengembalikan buku";
    }
} else {
    if (isset($_GET['id'])) {
        $id = $_GET['id'];
        $borrow_it = $db->query("INSERT INTO sewa(buku) VALUES('$id')");
    }
}

$borrow = $db->query("SELECT * FROM sewa");
$book = $db->query("SELECT * FROM sewa AS s LEFT JOIN buku b ON s.buku = b.id ORDER BY b.id DESC");
$book_count = $book->num_rows;

?>
<main>
    <?php if ($book_count > 0) : ?>
        <p>Jumlah barang
            <?= "(" . $book_count . ")" ?>
        </p>
        <table class="bordered">
            <thead>
                <tr>
                    <th>Sampul</th>
                    <th>Judul</th>
                    <th>Stok</th>
                    <th>Pengarang</th>
                    <th>Penerbit</th>
                    <th>Operasi</th>
                </tr>
            </thead>
            <tbody>
                <?php while ($data = $book->fetch_array(MYSQLI_BOTH)) : ?>
                    <tr>
                        <td>
                            <a href="./cover/<?= $data['cover'] ?>">
                                <img src="./thumbnail/<?= $data['cover'] ?>">
                            </a>
                        </td>
                        <td>
                            <?= ucwords($data['title']) ?>
                        </td>
                        <td>
                            <?= $data['quantity'] ?>
                        </td>
                        <td>
                            <?= ucwords($data['author']) ?>
                        </td>
                        <td>
                            <?= ucwords($data['publisher']) ?>
                        </td>
                        <td>
                            <a href="<?= $_SERVER['PHP_SELF'] ?>?id=<?= $data['id'] ?>&delete=true">
                                ❌ Batal
                            </a>
                        </td>
                    </tr>
                <?php endwhile ?>
            </tbody>
        </table>
    <?php else : ?>
        <p>Belum ada data yang ditambahkan</p>
    <?php endif ?>
</main>

<?php require_once "./footer.php" ?>