<?php $title = "Pencarian Buku" ?>
<?php require_once "./header.php" ?>
<header>
  <h2>Pencarian Buku</h2>
</header>
<main>
  <form method="POST" action="./index.php">
    <table>
      <tr>
        <td>
          <label>Judul</label>
        </td>
        <td>
          <input type="text" name="judul" id="judul">
        </td>
      </tr>
      <tr>
        <td>
          <label>Pengarang</label>
        </td>
        <td>
          <input type="text" name="pengarang" id="pengarang">
        </td>
      </tr>
      <tr>
        <td></td>
        <td>
          <button type="submit" name="search" value="search">
            Cari Buku
          </button>
        </td>
      </tr>
    </table>
  </form>
</main>
<?php require_once "./footer.php" ?>