<?php $title = "Peminjaman Buku" ?>
<?php require_once "./header.php" ?>
<?php require_once "./connections.php" ?>

<header>
    <h2>Peminjaman Buku</h2>
</header>
<main>
    <section>
        <main>
            <form method="POST" action="<?= $_SERVER['PHP_SELF'] ?>">
                <table class="undefault bordered">
                    <tr>
                        <th>Nama Pelanggan</th>
                        <td><input type="text" name="name" id="name"></td>
                    </tr>
                    <tr>
                        <th>Email</th>
                        <td><input type="email" name="email" id="email"></td>
                    </tr>
                    <tr>
                        <th>Nomor Telefon</th>
                        <td><input type="tel" name="telephone" id="telephone"></td>
                    </tr>
                    <tr>
                        <td></td>
                        <td><button type="submit" name="save" value="save">Simpan Peminjam</button></td>
                    </tr>
                </table>
            </form>
        </main>
    </section>
    <?php require_once "./bracket.php" ?>
</main>
<?php require_once "./footer.php" ?>