<?php $title = "Lihat Buku" ?>
<?php require_once "./header.php" ?>
<?php
$id = $_GET['id'] ?? die("Tidak dapat melakukan operasi lihat detail buku!");
require_once "./connections.php";
$get_book = $db->query("SELECT * FROM buku WHERE id = '$id'")->fetch_array(MYSQLI_BOTH);
?>
<header>
    <h2>Lihat Buku</h2>
</header>
<main>
    <table class="undefault bordered">
        <tr>
            <td colspan="2">
                <a href="./cover/<?= $get_book['cover'] ?>">
                    <center>
                        <img src=" ./cover/<?= $get_book['cover'] ?>" width="200">
                    </center>
                </a>
            </td>
        </tr>
        <tr>
            <th>Kode</th>
            <td>
                <?= ucwords($get_book['code']) ?>
            </td>
        </tr>
        <tr>
            <th>Judul</th>
            <td>
                <?= ucwords($get_book['title']) ?>
            </td>
        </tr>
        <tr>
            <th>Pengarang</th>
            <td>
                <?= ucwords($get_book['author']) ?>
            </td>
        </tr>
        <tr>
            <th>Penerbit</th>
            <td>
                <?= ucwords($get_book['publisher']) ?>
            </td>
        </tr>
        <tr>
            <th>Stok</th>
            <td>
                <?= $get_book['quantity'] ?>
            </td>
        </tr>
        <tr>
            <td></td>
            <td>
                <a href="./edit.php?id=<?= $book['id'] ?>">📝 Edit</a> &nbsp;
                <a href="./delete.php?id=<?= $book['id'] ?>">🗑️ Hapus</a>
            </td>
        </tr>
    </table>
</main>
<?php require_once "./footer.php" ?>