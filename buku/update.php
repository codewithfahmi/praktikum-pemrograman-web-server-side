<?php
function make_thumbnail($source, $destination)
{
    list($width, $height, $type) = getimagesize($source);

    $image_source = null;
    $thumbnail_width = 100;

    switch ($type) {
        case 1:
            $image_source = imagecreatefromgif($source);
            break;
        case 2:
            $image_source = imagecreatefromjpeg($source);
            break;
        case 3:
            $image_source = imagecreatefrompng($source);
            break;
    }

    $thumbnail_width_destination = 0;
    $thumbnail_height_destination = 0;

    if ($width > $thumbnail_width) {
        $thumbnail_width_destination = $thumbnail_width;
        $thumbnail_height_destination = round($thumbnail_width / $width * $height);
    } else {
        $thumbnail_width_destination = round($thumbnail_width / $height * $width);
        $thumbnail_height_destination = $thumbnail_width;
    }

    $thumbnail_destination = imagecreatetruecolor(
        $thumbnail_width_destination,
        $thumbnail_height_destination
    );
    imagecopyresampled(
        $thumbnail_destination,
        $image_source,
        0,
        0,
        0,
        0,
        $thumbnail_width_destination,
        $thumbnail_height_destination,
        $width,
        $height
    );

    imagejpeg($thumbnail_destination, $destination);
    imagedestroy($image_source);
    imagedestroy($thumbnail_destination);
}

if (isset($_POST['save'])) {
    $buku_code = trim($_POST['code']);
    $buku_title = trim($_POST['title']);
    $buku_quantity = trim($_POST['quantity']);
    $buku_author = trim($_POST['author']);
    $buku_publisher = trim($_POST['publisher']);
    $buku_old_cover = trim($_POST['old_cover']);
    $buku_cover = $_FILES['picture'];

    /** picture */
    $buku_cover_name = $buku_cover['name'];
    $buku_cover_tmp_name = $buku_cover['tmp_name'];
    $buku_cover_size = $buku_cover['size'];
    $buku_cover_type = $buku_cover['type'];
    $buku_cover_allow_types = [
        'image/jpeg',
        'image/pjepg',
        'image/jpg',
        'image/png',
        'image/gif'
    ];
    $buku_cover_size_max = 1500000;
    $buku_cover_dir = "./cover/" . $buku_cover_name;
    $buku_cover_thumb_dir = "./thumbnail/" . $buku_cover_name;

    $errors = array();

    if (empty($buku_code)) {
        $errors[] = "kode buku tidak boleh kosong";
    }
    if (empty($buku_title)) {
        $errors[] = "nama buku tidak boleh kosong";
    }
    if (empty($buku_quantity)) {
        $errors[] = "stok buku tidak boleh kosong";
    }
    if (empty($buku_author)) {
        $errors[] = "pengarang buku tidak boleh kosong";
    }
    if (empty($buku_publisher)) {
        $errors[] = "penerbit buku tidak boleh kosong";
    }

    if ($buku_cover_size > 0) {
        if ($buku_cover_size > $buku_cover_size_max)
            $errors[] = "ukuran sampul tidak boleh lebih dari 1.5MB";

        if (! in_array($buku_cover_type, $buku_cover_allow_types))
            $errors[] = "format sampul tidak didukung";

        if (! move_uploaded_file($buku_cover_tmp_name, $buku_cover_dir)) {
            $errors[] = "gagal unggah sampul";
        } else {
            unlink("./cover/$buku_old_cover");
            unlink("./thumbnail/$buku_old_cover");
            make_thumbnail($buku_cover_dir, $buku_cover_thumb_dir);
        }
    } else {
        $buku_cover_name = $buku_old_cover;
    }

    if (count($errors) > 0) {
        $title = "Kesalahan pada menyimpan buku";
        require_once "./header.php";
        $alert = "<h3>$title</h3>";
        $alert .= "<ul>";
        foreach ($errors as $error) {
            $alert .= "<li>" . ucfirst($error) . "</li>";
        }
        echo $alert . "</ul>";
        echo "<a href='./add.php'>Kembali</a>";
    } else {
        require_once "./connections.php";
        $update = $db->query("UPDATE buku SET
                            title       = '$buku_title',
                            author      = '$buku_author',
                            publisher   = '$buku_publisher',
                            quantity    = '$buku_quantity',
                            cover       = '$buku_cover_name'
                        ");

        if ($update) {
            $title = "Berhasil memperbarui buku";
            require_once "./header.php";
            echo "<header><h3>✅ buku \"$buku_title\" berhasil diperbarui</h3></header>";
            echo "<a href='./index.php'>Daftar buku</a>";
        } else {
            $title = "Gagal memperbarui buku";
            require_once "./header.php";
            echo "<header><h3>❌ Gagal menyimpan buku \"$buku_title\"</h3></header>";
            echo "<a href='./add.php'>Kembali</a>";
        }
    }
}
?>
<?php require_once "./footer.php" ?>