<?php $title = "Beranda"; ?>
<?php require_once "./header.php" ?>
<header>
  <h2>Daftar Buku Tersedia</h2>
</header>
<?php
require_once "./connections.php";
$sewa = $db->query("SELECT buku FROM sewa");
$sewa_list = [];
while ($data = $sewa->fetch_array()) {
  $sewa_list[] = $data['buku'];
}

$query = "SELECT * FROM buku WHERE 1=1";

if (count($sewa_list) > 0) {
  $sewa_list = implode(",", $sewa_list);
  $query .= " AND id NOT IN (" . $sewa_list . ")";
}

if (isset($_POST['search'])) {
  if (isset($_POST['judul']) && ! empty($_POST['judul'])) {
    $judul = $_POST['judul'];
    $query .= " AND title LIKE '%$judul%'";
  }

  if (isset($_POST['pengarang']) && ! empty($_POST['pengarang'])) {
    $pengarang = $_POST['pengarang'];
    $query .= " AND author LIKE '%$pengarang%'";
  }
}

$query .= " ORDER BY id DESC";

$get_book = $db->query($query);
$get_num_book = $get_book->num_rows;
?>
<main>
  <?php if ($get_num_book == 0) : ?>
    <p>Belum ada data buku yang ditambahkan</p>
  <?php else : ?>
    <p>Jumlah buku
      <?= "(" . $get_num_book . ")" ?>
    </p>
    <table class="bordered">
      <tr>
        <th>Foto</th>
        <th>Judul Buku</th>
        <th>Pengarang</th>
        <th>Operasi</th>
      </tr>
      <?php foreach ($get_book as $book) : ?>
        <tr>
          <td>
            <a href="./cover/<?= $book['cover'] ?>">
              <img src="./thumbnail/<?= $book['cover'] ?>">
            </a>
          </td>
          <td>
            <?= $book['title'] ?>
          </td>
          <td>
            <?= $book['author'] ?>
          </td>
          <td>
            <a href="./detail.php?id=<?= $book['id'] ?>">🕵 Lihat</a> &nbsp;
            <a href="./edit.php?id=<?= $book['id'] ?>">📝 Edit</a> &nbsp;
            <a href="./delete.php?id=<?= $book['id'] ?>">🗑️ Hapus</a> &nbsp;
            <a href="./bracket.php?id=<?= $book['id'] ?>">🫱 Pinjam</a>
          </td>
        </tr>
      <?php endforeach ?>
    </table>
  <?php endif ?>
</main>
<?php require_once "./footer.php" ?>