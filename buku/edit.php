<?php $title = "Edit Buku" ?>
<?php require_once "./header.php" ?>
<header>
    <h2>Edit Buku</h2>
</header>
<?php
$id = $_GET['id'] ?? die("tidak bisa melakukan operasi edit");
require_once "./connections.php";
$book = $db->query("SELECT * FROM buku WHERE id = '$id'")->fetch_array(MYSQLI_BOTH);
?>
<main>
    <form method="POST" action="./update.php" enctype="multipart/form-data">
        <input type="hidden" value="<?= $book['id'] ?>" name="id">
        <table class=" bordered undefault">
            <tr>
                <th>Kode</th>
                <td>
                    <input type="text" name="code" id="code" value="<?= $book['code'] ?>" readonly>
                </td>
            </tr>
            <tr>
                <th>Judul</th>
                <td><input type="text" name="title" id="title" value="<?= $book['title'] ?>"></td>
            </tr>
            <tr>
                <th>Stok</th>
                <td><input type="number" name="quantity" id="quantity" value="<?= $book['quantity'] ?>"></td>
            </tr>
            <tr>
                <th>Pengarang</th>
                <td><input type="text" name="author" id="author" value="<?= $book['author'] ?>"></td>
            </tr>
            <tr>
                <th>Penerbit</th>
                <td><input type="text" name="publisher" id="publisher" value="<?= $book['publisher'] ?>"></td>
            </tr>
            <tr>
                <th>Sampul</th>
                <td>
                    <input type="file" name="cover" id="cover"><br><br>
                    <input type="hidden" value="<?= $book['cover'] ?>" name="old_cover">
                    <img src="./thumbnail/<?= $book['cover'] ?>"><br>
                    <small>Foto Sampul Lama</small>
                </td>
            </tr>
            <tr>
                <td><!-- empty --></td>
                <td>
                    <button name="save" value="update">Perbarui buku</button>
                </td>
            </tr>
        </table>
    </form>
</main>
<script>
    const title = document.getElementById("title");
    const code = document.getElementById("code");
    title.addEventListener("keyup", function (event) {
        const splitCode = this.value.split(" ");
        let setCode = [];
        splitCode.forEach(element => {
            setCode.push(element[0]);
        });

        code.setAttribute("value", setCode.join("").toLocaleUpperCase());
    })
</script>
<?php require_once "./footer.php" ?>