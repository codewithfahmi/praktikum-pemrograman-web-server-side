<?php $title = "Edit Barang"; ?>
<?php require_once "./header.php"; ?>
<header>
  <h2>Edit Barang</h2>
</header>
<?php
$id = $_GET["id"] ?? die("tidak bisa melakukan operasi edit");
require_once "./connections.php";
$barang = $db
    ->query("SELECT * FROM barang WHERE id = '$id'")
    ->fetch_array(MYSQLI_BOTH);
?>
<main>
  <form method="POST" action="./update.php" enctype="multipart/form-data">
    <input type="hidden" value="<?= $barang["id"] ?>" name="id">
    <table class=" bordered undefault">
      <tr>
        <th>Nama Barang</th>
        <td><input type="text" name="name" id="name" value="<?= $barang[
            "name"
        ] ?>"></td>
      </tr>
      <tr>
        <th>Harga</th>
        <td><input type="tel" name="price" id="price" value="<?= $barang[
            "price"
        ] ?>"></td>
      </tr>
      <tr>
        <th>Stok Jual</th>
        <td><input type="number" name="quantity" id="quantity" value="<?= $barang[
            "quantity"
        ] ?>"></td>
      </tr>
      <tr>
        <th>Foto Produk</th>
        <td>
          <input type="file" name="picture" id="picture"><br><br>
          <input type="hidden" value="<?= $barang[
              "picture"
          ] ?>" name="old_picture">
          <img src="./thumbnail/<?= $barang["picture"] ?>"><br>
          <small>Foto produk lama</small>
        </td>
      </tr>
      <tr>
        <td><!-- empty --></td>
        <td>
          <button name="save" value="update">Perbarui Barang</button>
        </td>
      </tr>
    </table>
  </form>
</main>
<?php require_once "./footer.php"; ?>
