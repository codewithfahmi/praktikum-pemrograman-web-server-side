<?php require_once "./connections.php"; ?>

<?php $title = "Keranjang Belanja"; ?>
<?php require_once "./header.php"; ?>
<header>
  <h2>Keranjang Belanja</h2>
</header>
<?php
$shopping_cart = 0;
if (isset($_COOKIE["keranjang_belanja"])) {
    $shopping_cart = $_COOKIE["keranjang_belanja"];
}
if (isset($_GET["id"])) {
    $id = $_GET["id"];
    $shopping_cart = str_replace("," . $id, "", $shopping_cart);
    setcookie("keranjang_belanja", $shopping_cart, time() + 3600);
}
$keyword = $_POST["keyword"] ?? null;
$query = "SELECT * FROM barang WHERE 1=1 AND id in (" . $shopping_cart . ")";
if ($keyword != null) {
    $query .= " AND name LIKE '%" . $keyword . "%'";
}
$query .= " ORDER BY id DESC";
$get_data_barang = $db->query($query);
$get_count_barang = $get_data_barang->num_rows;
?>
<main>
  <?php if ($get_count_barang > 0): ?>
    <p>Jumlah barang
      <?= "(" . $get_count_barang . ")" ?>
    </p>
    <table class="bordered">
      <thead>
        <tr>
          <th>Foto</th>
          <th>Nama Barang</th>
          <th>Harga Jual</th>
          <th>Stok</th>
          <th>Operasi</th>
        </tr>
      </thead>
      <tbody>
        <?php foreach ($get_data_barang as $data): ?>
          <tr>
            <td>
              <a href="./picture/<?= $data["picture"] ?>">
                <img src="./thumbnail/<?= $data["picture"] ?>">
              </a>
            </td>
            <td>
              <?= ucwords($data["name"]) ?>
            </td>
            <td>
              Rp.
              <?= number_format($data["price"]) ?>
            </td>
            <td>
              <?= $data["quantity"] ?>
            </td>
            <td>
              <a href="<?= $_SERVER["PHP_SELF"] . "?id=" . $data["id"] ?>">
                ❌ Batal
              </a>
            </td>
          </tr>
        <?php endforeach; ?>
      </tbody>
    </table>
  <?php else: ?>
    <?php if (isset($_POST["keyword"])): ?>
      <p>Data yang dicari tidak ditemukan</p>
    <?php else: ?>
      <p>Belum ada data barang yang ditambahkan</p>
    <?php endif; ?>
  <?php endif; ?>
</main>

<?php require_once "./footer.php"; ?>
