<?php $title = "Lihat Barang"; ?>
<?php require_once "./header.php"; ?>
<header>
  <h1>Lihat Barang</h1>
</header>
<?php
require_once "./connections.php";
$id = $_GET["id"] ?? die("tidak dapat melakukan operasi lihat barang");
$barang = $db
    ->query("SELECT * FROM barang WHERE id = '$id'")
    ->fetch_array(MYSQLI_BOTH);
?>
<main>
  <table class="bordered undefault">
    <tr>
      <td colspan="2" style="text-align: center">
        <a href="./picture/<?= $barang["picture"] ?>">
          <img src="./picture/<?= $barang["picture"] ?>" width="250">
        </a>
      </td>
    </tr>
    <tr>
      <th>Nama Barang</th>
      <td>
        <?= ucwords($barang["name"]) ?>
      </td>
    </tr>
    <tr>
      <th>Harga</th>
      <td>
        Rp.
        <?= number_format($barang["quantity"]) ?>
      </td>
    </tr>
    <tr>
      <th>Stok Jual</th>
      <td>
        <?= $barang["quantity"] ?>
      </td>
    </tr>
    <tr>
      <td></td>
      <td>
        <a href="./edit.php?id=<?= $data["id"] ?>">📝 Edit</a> &nbsp;
        <a href="./delete.php?id=<?= $data["itd"] ?>">🗑️ Hapus</a>
      </td>
    </tr>
  </table>
</main>
<?php require_once "./footer.php"; ?>
