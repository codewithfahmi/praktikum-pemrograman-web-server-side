<?php
function make_thumbnail($source, $destination)
{
    list($width, $height, $type) = getimagesize($source);

    $image_source = null;
    $thumbnail_width = 100;

    switch ($type) {
        case 1:
            $image_source = imagecreatefromgif($source);
            break;
        case 2:
            $image_source = imagecreatefromjpeg($source);
            break;
        case 3:
            $image_source = imagecreatefrompng($source);
            break;
    }

    $thumbnail_width_destination = 0;
    $thumbnail_height_destination = 0;

    if ($width > $thumbnail_width) {
        $thumbnail_width_destination = $thumbnail_width;
        $thumbnail_height_destination = round(
            ($thumbnail_width / $width) * $height
        );
    } else {
        $thumbnail_width_destination = round(
            ($thumbnail_width / $height) * $width
        );
        $thumbnail_height_destination = $thumbnail_width;
    }

    $thumbnail_destination = imagecreatetruecolor(
        $thumbnail_width_destination,
        $thumbnail_height_destination
    );
    imagecopyresampled(
        $thumbnail_destination,
        $image_source,
        0,
        0,
        0,
        0,
        $thumbnail_width_destination,
        $thumbnail_height_destination,
        $width,
        $height
    );

    imagejpeg($thumbnail_destination, $destination);
    imagedestroy($image_source);
    imagedestroy($thumbnail_destination);
}

if (isset($_POST["save"])) {
    $barang_name = trim($_POST["name"]);
    $barang_price = trim($_POST["price"]);
    $barang_quantity = trim($_POST["quantity"]);
    $barang_picture = $_FILES["picture"];

    /** picture */
    $barang_picture_name = $barang_picture["name"];
    $barang_picture_tmp_name = $barang_picture["tmp_name"];
    $barang_picture_size = $barang_picture["size"];
    $barang_picture_type = $barang_picture["type"];
    $barang_picture_allow_types = [
        "image/jpeg",
        "image/pjepg",
        "image/jpg",
        "image/png",
        "image/gif",
    ];
    $barang_picture_size_max = 1500000;
    $barang_picture_dir = "./picture/" . $barang_picture_name;
    $barang_picture_thumb_dir = "./thumbnail/" . $barang_picture_name;

    $errors = [];

    if (empty($barang_name)) {
        $errors[] = "nama barang tidak boleh kosong";
    }
    if (empty($barang_price)) {
        $errors[] = "harga barang tidak boleh kosong";
    }
    if (empty($barang_quantity)) {
        $errors[] = "stok barang tidak boleh kosong";
    }
    if ($barang_picture_size == 0) {
        $errors[] = "foto barang tidak boleh kosong";
    }
    if ($barang_picture_size > 0) {
        if ($barang_picture_size > $barang_picture_size_max) {
            $errors[] = "ukuran foto tidak boleh lebih dari 1.5MB";
        }

        if (!in_array($barang_picture_type, $barang_picture_allow_types)) {
            $errors[] = "format foto tidak didukung";
        }

        if (
            !move_uploaded_file($barang_picture_tmp_name, $barang_picture_dir)
        ) {
            $errors[] = "gagal unggah foto";
        } else {
            make_thumbnail($barang_picture_dir, $barang_picture_thumb_dir);
        }
    }

    if (count($errors) > 0) {
        $title = "Kesalahan pada menyimpan barang";
        require_once "./header.php";
        $alert = "<h3>$title</h3>";
        $alert .= "<ul>";
        foreach ($errors as $error) {
            $alert .= "<li>" . ucfirst($error) . "</li>";
        }
        echo $alert . "</ul>";
        echo "<a href='./add.php'>Kembali</a>";
    } else {
        require_once "./connections.php";
        $save = $db->query("INSERT INTO barang(name, price, quantity, picture)
															 VALUES(
																	'$barang_name',
																	'$barang_price',
																	'$barang_quantity',
																	'$barang_picture_name')");

        if ($save) {
            $title = "Berhasil menyimpan barang";
            require_once "./header.php";
            echo "<header><h3>✅ Barang \"$barang_name\" berhasil disimpan</h3></header>";
            echo "<a href='./index.php'>Daftar Barang</a>";
        } else {
            $title = "Gagal menyimpan barang";
            require_once "./header.php";
            echo "<header><h3>❌ Gagal menyimpan barang \"$barang_name\"</h3></header>";
            echo "<a href='./add.php'>Kembali</a>";
        }
    }
}
?>
<?php require_once "./footer.php"; ?>
