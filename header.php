<!DOCTYPE html>
<html>

<head>
    <title>
        <?= $title ?>
    </title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="x-ua-comptaible" content="ie=edge">
    <meta charset="utf-8">
    <link rel="stylesheet" href="./css/style.css">
</head>

<body>
    <?php require_once "./navigation.php"; ?>
    <div class="container">