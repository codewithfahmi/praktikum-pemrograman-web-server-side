<?php $title = "Hapus Barang"; ?>
<?php require_once "./header.php"; ?>
<?php
require_once "./connections.php";
$id = $_GET["id"] ?? die("tidak dapat melakukan operasi lihat barang");
$barang = $db
    ->query("SELECT * FROM barang WHERE id = '$id'")
    ->fetch_array(MYSQLI_BOTH);
?>

<?php if (isset($_GET["delete"])) {
    $is_delete = $_GET["delete"];
    if ($is_delete) {
        unlink("./picture/" . $barang["picture"]);
        unlink("./thumbnail/" . $barang["picture"]);
        $delete = $db->query("DELETE FROM barang WHERE id = '$id'");
        if ($delete) {
            echo "<header><h3>✅ Barang berhasil dihapus</h3></header>";
            echo "<a href='./index.php'>Daftar Barang</a>";
        }
    }
} else {
     ?>
  <header>
    <h2>Hapus Barang</h2>
  </header>
  <main>
    <table class="bordered undefault">
      <tr>
        <td colspan="2" style="text-align: center">
          <a href="./picture/<?= $barang["picture"] ?>">
            <img src="./picture/<?= $barang["picture"] ?>" width="250">
          </a>
        </td>
      </tr>
      <tr>
        <th>Nama Barang</th>
        <td>
          <?= ucwords($barang["name"]) ?>
        </td>
      </tr>
      <tr>
        <th>Harga</th>
        <td>
          Rp.
          <?= number_format($barang["price"]) ?>
        </td>
      </tr>
      <tr>
        <th>Stok Jual</th>
        <td>
          <?= $barang["quantity"] ?>
        </td>
      </tr>
      <tr>
        <th>Hapus barang ini?</th>
        <td>
          <a href="./delete.php?id=<?= $barang["id"] ?>&delete=true">
            🗑️ Hapus Barang
          </a> &nbsp;
          <a href="./index.php">
            ❌ Batal
          </a>
        </td>
      </tr>
    </table>
  </main>
  <?php
} ?>

<?php require_once "./footer.php"; ?>
